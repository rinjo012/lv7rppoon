﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7RPPOON
{
    class Program
    {
        static void Main(string[] args)
        {     //1.
            double[] ArrayToSequence = new double[] { -12.7, 23, 104, -84, -12.5, 100, 400, 10, 65, 1 };
            NumberSequence numberSequence = new NumberSequence(ArrayToSequence);
            BubbleSort bubbleSort = new BubbleSort();
            numberSequence.SetSortStrategy(bubbleSort);

            Console.WriteLine("Sequence before sorting:\n" + numberSequence.ToString());

            numberSequence.Sort();
            Console.WriteLine("Sequence after sorting:\n" + numberSequence.ToString());

            //2.
            LinearSearch linearSearch = new LinearSearch();
            numberSequence.SetSearchStrategy(linearSearch);
            Console.WriteLine("Number 100 is at index number: "+numberSequence.Search(100));

            
            //3.
            SystemDataProvider DataProvider = new SystemDataProvider();
            DataProvider.Attach(new ConsoleLogger());
           
            while (true)
            {
                DataProvider.GetCPULoad();
                DataProvider.GetAvailableRAM();
                System.Threading.Thread.Sleep(1000);
            }

        }
    }
}
