﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV7RPPOON
{
   abstract class SearchStrategy
    {
        public abstract int Search(double number, double[] array);

    }
}
